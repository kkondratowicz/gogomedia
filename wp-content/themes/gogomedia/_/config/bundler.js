const webpack = require('webpack');
const path = require('path');

module.exports = {
    context: path.resolve(__dirname, './'),
    entry: {
        main: ['../js/src/main.js']
    },
    output: {
        path: path.resolve(__dirname, '../js/dist'),
        filename: '[name].js',

    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules\/(?!(dom7|ssr-window|swiper|jquery)\/).*/,
            loader: 'babel-loader',
        }]
    },
    optimization: {
        minimize: true,
    },
    plugins: [],
    watch: false,
    devtool: 'source-map',
    mode: 'production',
};