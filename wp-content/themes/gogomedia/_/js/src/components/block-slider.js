import Core from "../core";
import SliderDefault from "../lib/slider";

export default class BlockSlider {
    constructor(options = {}) {
        this.element = options.element;

        if(!this.element) {
            console.error('Parameter 1 is not of type "Node" or "NodeList"');

            return;
        }

        this.instances = [];

        if(this.element instanceof Node) {
            this.instances.push(new SliderDefault(this.element, this.getSliderOptions(this.element)));
        }

        if(this.element instanceof NodeList) {
            this.element.forEach(item => {
                this.instances.push(new SliderDefault(item, this.getSliderOptions(item)));
            });
        }
    }

    isSliderDisabledOnDesktop(element) {
        return element ? element?.classList.contains('slider--disabled-on-desktop') : false
    }

    getSliderOptions(element = null) {
        if(!element) {
            return {};
        }

        return {
            allowTouchMove: true,
            slidesPerView: 1,
            spaceBetween: 20,
            navigation: {
                nextEl: element.querySelector('.slider__button--next'),
                prevEl: element.querySelector('.slider__button--prev')
            },
            breakpoints: {
                1024: {
                    allowTouchMove: !this.isSliderDisabledOnDesktop(element),
                    slidesPerView: 3,
                },

                650: {
                    slidesPerView: 2
                }
            }
        }
    }

    /*
    destroy() {
        this.instances.forEach(instance => instance?.destroy());
    }
    */
}