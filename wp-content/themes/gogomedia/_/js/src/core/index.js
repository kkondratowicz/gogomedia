export default class Core {
    static components(arr = []) {
        const componentsInstances = [];

        arr.forEach((component) => {
            let componentInstance = null;

            if(Array.isArray(component) && component.length > 0) {
                const _component = component[0];
                const _args = component[1] ?? {};
                
                componentInstance = new _component(_args);
            } else {
                componentInstance = new component();
            }

            componentsInstances.push(componentInstance);
        });

        return componentsInstances;
    }

    static mergeOptions(customOptions = {}, defaultOptions = {}) {
        return {
            ...defaultOptions,
            ...customOptions
        }
    }

    static emitEvent(type, el = window, detail = {}) {
        if(!type) {
            return;
        }

        const event = new CustomEvent(type, {
            bubbles: true,
            cancelable: true,
            detail: detail
        });

        el.dispatchEvent(event);
    }
}