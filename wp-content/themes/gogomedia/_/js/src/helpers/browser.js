const browser = {
    isIe: () => {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) return true;
        else return false;
    },

	isEdge: () => {
		if (/Edge\/\d./i.test(navigator.userAgent)) {
			return true;
		}

		return false;
    },
    
    MSBrowser: () => {
        if(browser.isIe() || browser.isEdge()) {
            return true;
        }

        return false;
    }
}

export default browser;