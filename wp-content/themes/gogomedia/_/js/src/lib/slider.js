import Swiper, { Keyboard, Lazy, A11y, Navigation } from 'swiper';

Swiper.use([
    Keyboard,
    Lazy,
    A11y,
    Navigation
]);

export default class SliderDefault {
    constructor(element = null, options = {}) {
        if(element instanceof Node) {
            this.element = element;
            this.slides = this.element.querySelectorAll('.slider__slide');
            this.options = {
                loop: false,
                keyboard: true,
                effect: 'slider',
                speed: 800,
                autoplay: false,
                preloadImages: false,
                lazy: false,
                ...options
            };
    
            this.prepareDom();
            this.createSlider();
            this.bind();
        } else {
            console.error('Parameter 1 is not of type "Node"');
        }
    }

    get swiperInstance() {
        return this.swiper;
    }
    
    prepareDom() {
        const container = document.createElement('div');
        const wrapper = document.createElement('div');
        const childrenDocumentFragment = document.createDocumentFragment();

        container.classList.add('swiper-container');
        wrapper.classList.add('swiper-wrapper');

        this.slides.forEach((slide, i) => {
            slide.classList.add('swiper-slide');
            slide.dataset.slideNumber = i;
            
            childrenDocumentFragment.appendChild(slide);
        });
        
        wrapper.appendChild(childrenDocumentFragment);
        container.appendChild(wrapper);
        
        this.element.appendChild(container);
        
        this.swiperContainer = this.element.querySelector('.swiper-container');
    }
    
    createSlider() {
        this.swiper = new Swiper(this.swiperContainer, this.options);
    }

    bind() {}

    destroy() {
        this?.swiper?.destroy?.();
    }
}