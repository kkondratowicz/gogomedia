import Core from './core';

import BlockSlider from './components/block-slider';

Core.components([
    [BlockSlider, {
        element: document.querySelectorAll('[data-block-slider]')
    }]
]);