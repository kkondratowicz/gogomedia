<?php

/* ------------------------------------------------------------------------- */

setlocale(LC_TIME, get_locale());

/* ------------------------------------------------------------------------- */

load_theme_textdomain( 'gogomedia', get_template_directory() . '/languages');

/* ------------------------------------------------------------------------- */

add_filter('show_admin_bar', '__return_false');

/* ------------------------------------------------------------------------- */

add_theme_support('html5');
add_theme_support('post-thumbnails');
add_theme_support('automatic-feed-links');
add_theme_support('structured-post-formats', array('link', 'video'));
add_theme_support('post-formats', array(
	'aside', 'audio', 'chat', 'gallery', 'image', 'quote', 'status')
);

/* ------------------------------------------------------------------------- */

function svg_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	
	return $mimes;
}

add_filter('upload_mimes', 'svg_mime_types');

/* ------------------------------------------------------------------------- */

//https://developer.wordpress.org/reference/functions/wp_title/#comment-375
function _wp_title($title, $sep) {
	global $paged, $page;

	if (is_feed()) {
		return $title;
	}
	// Add the site name.
	$title .= get_bloginfo('name');
	// Add the site description for the home/front page.
	$site_description = get_bloginfo('description', 'display');

	if ($site_description && (is_home() || is_front_page())) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ($paged >= 2 || $page >= 2) {
		$title = "$title $sep " . sprintf(__('Page %s', 'gogomedia'), max($paged, $page));
	}

	return $title;
}

add_filter( 'wp_title', '_wp_title', 10, 2 );

/* ------------------------------------------------------------------------- */

function tpl_enqueue_style($handle, $src, $deps = array(), $media = 'all', $is_child_theme = false) {
	if ($is_child_theme) {
		$path = get_stylesheet_directory() . '/'. ltrim($src, '/');
		$src = get_stylesheet_directory_uri() . '/'. ltrim($src, '/');
	} else {
		$path = get_template_directory() . '/'. ltrim($src, '/');
		$src = get_template_directory_uri() . '/'. ltrim($src, '/');
	}

	$ver = filemtime($path);

	wp_enqueue_style($handle, $src, $deps, $ver, $media);
}

/* ------------------------------------------------------------------------- */

function tpl_enqueue_script($handle, $src, $deps = array(), $in_footer = true, $is_child_theme = false) {
	if ($is_child_theme) {
		$path = get_stylesheet_directory() . '/'. ltrim($src, '/');
		$src = get_stylesheet_directory_uri() . '/'. ltrim($src, '/');
	} else {
		$path = get_template_directory() . '/'. ltrim($src, '/');
		$src = get_template_directory_uri() . '/'. ltrim($src, '/');
	}

	$ver = filemtime($path);

	wp_enqueue_script($handle, $src, $deps, $ver, $in_footer);
}

/* ------------------------------------------------------------------------- */

function inline_svg($src, $echo = true) {
	// $src = get_theme_file_uri($src);
	$src = __DIR__ . $src;

	if($echo) {
		echo file_get_contents($src);
	} else {
		return file_get_contents($src);
	}
}

/* ------ EOF -------------------------------------------------------------- */