<?php /* Template Name: Strona Główna */ ?>
<?php get_header(); ?>


<article class="home">
<?php 
if ( have_posts() ) { 
    while ( have_posts() ) : the_post();
        the_content();
    endwhile;
}
?>
</article>


<?php get_footer(); ?>
