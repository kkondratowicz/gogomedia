<?php

/* ------------------------------------------------------------------------- */

require_once '_settings.php';

/* ------------------------------------------------------------------------- */

function scripts_styles() {
	global $wp_styles;

	// Load Comments
	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}

    tpl_enqueue_style('screen', '_/css/main.css');

    wp_deregister_script('jquery');
    
    $libs = json_decode(file_get_contents(__DIR__ . '/scripts.json'));

	foreach($libs as $key => $lib) {
        if(preg_match("/^(http|https):\/\//i", $lib)) {
            wp_enqueue_script($key, $lib, [], null, true);
        } else {
            tpl_enqueue_script($key, $lib, []);  
        }
	}
}

add_action('wp_enqueue_scripts', 'scripts_styles');

/* ------------------------------------------------------------------------- */

function defer_attribute($tag, $handle) {
    $libs = json_decode(file_get_contents(__DIR__ . '/scripts.json'));

    foreach($libs as $key => $lib) {
		if($key === $handle) {
			$tag = str_replace(' src', ' defer="defer" src', $tag);
		}
	}

	return $tag;
}

add_filter('script_loader_tag', 'defer_attribute', 10, 2);

/* ------------------------------------------------------------------------- */

function remove_admin_options() {
    remove_menu_page('edit.php');
    // remove_menu_page('add.php');
    remove_menu_page('edit-comments.php');
    remove_submenu_page('themes.php', 'widgets.php');
    remove_submenu_page('plugins.php', 'plugin-editor.php');
}

add_action('admin_menu', 'remove_admin_options');

/* ------------------------------------------------------------------------- */

remove_action( 'wp_head', 'wp_generator' );

/* ------------------------------------------------------------------------- */

// https://github.com/wp-plugins/sierotki/blob/master/vendor/iworks/orphan.php#L79
function orphans($content) {
	if ( empty( $content ) ) {
        return;
    }
    $therms = array (
        'al.', 'ale', 'ależ',
        'b.', 'bł.', 'bm.', 'bp', 'br.', 'by', 'bym', 'byś',
        'cyt.', 'cz.', 'czyt.',
        'dn.', 'do', 'doc.', 'dr', 'ds.', 'dyr.', 'dz.',
        'fot.',
        'gdy', 'gdyby', 'gdybym', 'gdybyś', 'gdyż', 'godz.',
        'im.', 'inż.',
        'jw.',
        'kol.', 'komu', 'ks.', 'która', 'którego', 'której', 'któremu', 'który', 'których', 'którym', 'którzy',
        'lic.',
        'max', 'mgr', 'm.in.', 'min', 'moich', 'moje', 'mojego', 'mojej', 'mojemu', 'mój', 'mych', 'na', 'nad', 'np.','nt.', 'nw.',
        'nr', 'nr.', 'nru', 'nrowi', 'nrem', 'nrze', 'nrze', 'nry', 'nrów', 'nrom', 'nrami', 'nrach',
        'od', 'oraz', 'os.',
        'p.', 'pl.', 'pn.', 'po', 'pod', 'pot.', 'prof.', 'przed', 'pt.', 'pw.', 'pw.',
        'śp.', 'św.',
        'tamtej', 'tamto', 'tej', 'tel.', 'tj.', 'to', 'twoich', 'twoje', 'twojego', 'twojej', 'twój', 'twych',
        'ul.',
        'we', 'wg', 'woj.',
        'za', 'ze',
        'że', 'żeby', 'żebyś',
    );
    $own_orphans = trim( get_option('iworks_orphan_own_orphans', '' ), ' \t,');
    if ( $own_orphans ) {
        $own_orphans = preg_replace('/\,\+/', ',', $own_orphans);
        $therms = array_merge( $therms, preg_split('/,[ \t]*/', strtolower( $own_orphans ) ) );
    }
    $therms = apply_filters('iworks_orphan_therms', $therms);
    /**
     * base therms replace
     */
    $re = '/^([aiouwz]|'.preg_replace('/\./', '\.', implode('|', $therms)).') +/i';
    $content = preg_replace( $re, "$1$2&nbsp;", $content );
    /**
     * replace space in numbers
     */
    $content = preg_replace('/(\d) (\d)/', "$1&nbsp;$2", $content );
    /**
     * single letters
     */
    $re = '/([ >\(]+)([aiouwz]|'.preg_replace('/\./', '\.', implode('|', $therms)).') +/i';
    $content = preg_replace( $re, "$1$2&nbsp;", $content );
    /**
     * single letter after previous orphan
     */
    $re = '/(&nbsp;)([aiouwz]) +/i';
    $content = preg_replace( $re, "$1$2&nbsp;", $content );
    /**
     * polish year after number
     */
    $content = preg_replace('/(\d+) (r\.)/', "$1&nbsp;$2", $content);
    /**
     * return
     */
    return $content;
}

function _acf_format_value( $value, $post_id, $field ) {

	$value = orphans($value);
	return $value;
}

add_filter('acf/format_value/type=text', '_acf_format_value', 10, 3);
add_filter('acf/format_value/type=wysiwyg', '_acf_format_value', 10, 3);
add_filter('acf/format_value/type=textarea', '_acf_format_value', 10, 3);

/* ------------------------------------------------------------------------- */

add_filter('big_image_size_threshold', '__return_false');

/* ------------------------------------------------------------------------- */

function acf_init_blocks() {
    if(function_exists('acf_register_block_type')) {
        acf_register_block_type([
            'name'              => 'slider',
            'title'             => 'Slider',
            'render_template'   => 'partials/blocks/slider.php',
            'category'          => 'widgets',
            'icon'              => 'slides',
            'keywords'          => ['slider', 'gallery', 'image'],
        ]);
    }
}

add_action('acf/init', 'acf_init_blocks');

/* ------ EOF -------------------------------------------------------------- */
