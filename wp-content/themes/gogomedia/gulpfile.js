(function(gulp) {
    'use strict';
    
    const sass = require('gulp-sass');
    const autoprefixer = require('gulp-autoprefixer');
    const cssmin = require('gulp-cssmin');
    const pixrem = require('gulp-pixrem');
    const webpack = require('webpack');
    const gulpWebpack = require('gulp-webpack');
    const webpackConfig = require('./_/config/bundler.js');
    const browserSync = require('browser-sync').create();
    const browserSupport = ['last 2 version', 'safari >= 5', 'ie >= 11', 'ios >= 8', 'android >= 4.4'];

    sass.compiler = require('node-sass');

    gulp.task('js', () => {
        return gulp.src('_/js/src/main.js')
            .pipe(gulpWebpack(webpackConfig, webpack))
            .pipe(gulp.dest('_/js/dist'));
    });

    gulp.task('css', () => {
        return gulp.src('_/sass/main.scss')
            .pipe(sass().on('error', sass.logError))
            .pipe(autoprefixer({
                browsers: browserSupport,
                cascade: false
            }))
            .pipe(cssmin())
            .pipe(pixrem('10px', {
                browsers: browserSupport 
            }))
            .pipe(gulp.dest('_/css'));
    });

    gulp.task('watch', (done) => {
        gulp.watch('_/sass/**/*.scss', gulp.series(['css']));
        gulp.watch(['_/js/src/**/*.js', '_/js/libs/*.js'], gulp.series(['js']));

        browserSync.init({
            proxy: "localhost/gogomedia",
            injectChanges: true,
            notify: false
        });

        gulp.watch(['_/css/*.css', '_/js/dist/*.js', '*.php', 'partials/**']).on('change', browserSync.reload);

        done();
    });

    gulp.task('default', gulp.series(['js', 'css', 'watch']));
    
}(require('gulp')));