<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <title><?php wp_title( '-', true, 'right' ); ?></title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        
        <?php wp_head(); ?>
    </head>

    <body 
        class="<?php echo join(' ', get_body_class()); ?>" 
        data-base-href="<?php echo site_url('/'); ?>" 
        data-home-url="<?php echo esc_url(home_url()); ?>" 
        data-current-url="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" 
        data-assets-href="<?php bloginfo('template_directory'); ?>/">
            
        <header class="header">
            <div class="header__wrapper">
                <h1 class="header__logo">
                    <a href="<?php echo esc_url(home_url('/')); ?>" class="header__logo-link">
                        <?php inline_svg('/_/img/logo.svg'); ?>
                        <span class="visually-hidden"><?php echo bloginfo('name'); ?></span>
                    </a>
                </h1>
        </header>

        <main class="main">
        