<?php 
    get_header();

    if(have_posts()): while (have_posts()): the_post(); 
?>
<article class="section section--page">
    <div class="page page--single">
        <div class="page__wrapper">
            <h1 class="page__heading"><?php the_title(); ?></h1>
            <div class="page__content"><?php the_content(); ?></div>
        </div>
    </div>
</article>
<?php 
    endwhile; endif; 

    get_footer(); 
?>