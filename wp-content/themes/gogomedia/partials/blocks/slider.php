<?php 
/**
* Block Name: Slider
*/

$slides = get_field('slides');
$id = "slider{$block['id']}";
$alignClass = $block['align'] ? "align-{$block['align']}" : '';
$additionalClass = $block['className'] ?? '';

if($slides):
?>

<div class="slider  <?php   echo count($slides) < 4 ? 'slider--disabled-on-desktop' : '';
                            echo $alignClass; 
                            echo $additionalClass; ?>" data-block-slider>
    <?php foreach($slides as $slide): extract($slide); $heading = $copy['heading']; $text = $copy['text']; ?>
    <div class="slider__slide slide">
        <?php if($heading): ?>
        <h3 class="slide__heading"><?php echo $heading; ?></h3>
        <?php endif; ?>

        <?php if($text): ?>
        <p class="slide__text"><?php echo $text; ?></p>
        <?php endif; ?>

        <?php if($icon): ?>
        <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt'] ?? ''; ?>" class="slide__icon">
        <?php endif; ?>
    </div>
    <?php endforeach; ?>

    <button class="slider__button slider__button--prev">
        <?php inline_svg('/_/img/slider-arrow.svg'); ?>
    </button>
    <button class="slider__button slider__button--next">
        <?php inline_svg('/_/img/slider-arrow.svg'); ?>
    </button>
</div>

<?php endif; ?>